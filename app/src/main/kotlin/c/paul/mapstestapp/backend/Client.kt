package c.paul.mapstestapp.backend

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Paul Chernenko
 * on 22.06.2018.
 */
object Client {

    private const val BASE_URL = "http://www.json-generator.com/"

    private var retrofit: Retrofit? = null

    @Synchronized
    fun getInstance(): Api {
        if (retrofit == null)
            retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(createClient())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(createGson()))
                    .build()
        return retrofit!!.create<Api>(Api::class.java)
    }

    private fun createClient(): OkHttpClient {
        return OkHttpClient().newBuilder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build()
    }

    private fun createGson(): Gson {
        return GsonBuilder()
                .setDateFormat("MM/dd/yyyy")
                .create()
    }
}