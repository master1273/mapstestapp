package c.paul.mapstestapp.backend.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Paul Chernenko
 * on 22.06.2018.
 */
data class Point(

        @SerializedName("id")
        val id: Int,

        @SerializedName("lat")
        val latitude: Double,

        @SerializedName("lng")
        val longitude: Double,

        @SerializedName("title")
        val title: String,

        @SerializedName("description")
        val description: String
)