package c.paul.mapstestapp.backend

import c.paul.mapstestapp.backend.model.Point
import io.reactivex.Observable
import retrofit2.http.*

/**
 * Created by Paul Chernenko
 * on 22.06.2018.
 */
interface Api {

    @Headers("Content-Type: application/json")
    @GET("api/json/get/bPvorlFkwO")
    fun getMarkers(@Query("indent") indent: Int): Observable<List<Point>>
}