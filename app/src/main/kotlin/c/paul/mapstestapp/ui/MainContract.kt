package c.paul.mapstestapp.ui

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

/**
 * Created by Paul Chernenko
 * on 22.06.2018.
 */
object MainContract {

    interface View {
        fun initMap()
        fun showLoadingError()
        fun addMarker(id: Int, markerOptions: MarkerOptions)
        fun showDescription(title: String, description: String)
        fun centerMap(position: LatLng)
        fun hideDescription()
    }

    interface Presenter {
        fun attach(view: View)
        fun detach()
        fun loadMarkers()
        fun mapReady()
        fun onMarkerClick(id: Int)
        fun onMapClick()
    }

}