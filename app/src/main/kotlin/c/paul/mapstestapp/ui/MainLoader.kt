package c.paul.mapstestapp.ui

import android.content.Context
import android.support.v4.content.Loader

/**
 * Created by Paul Chernenko
 * on 23.06.2018.
 */
class MainLoader(context: Context, private val presenter: MainPresenter) : Loader<MainPresenter>(context) {

    override fun onStartLoading() {
        deliverResult(presenter)
    }
}