package c.paul.mapstestapp.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import android.support.v7.app.AlertDialog
import android.view.View
import c.paul.mapstestapp.R
import com.google.android.gms.maps.CameraUpdateFactory

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_main.*
import com.google.android.gms.maps.model.MarkerOptions

class MainActivity : AppCompatActivity(), OnMapReadyCallback, MainContract.View, LoaderManager.LoaderCallbacks<MainPresenter> {

    private lateinit var mMap: GoogleMap
    private lateinit var presenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // start the loader to load the presenter
        supportLoaderManager.initLoader(1001, null, this)
    }

    override fun onResume() {
        super.onResume()
        presenter.attach(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.detach()
    }

    /**
     * Loader methods
     */
    override fun onCreateLoader(id: Int, args: Bundle?): Loader<MainPresenter> {
        return MainLoader(this, MainPresenter())
    }

    override fun onLoadFinished(loader: Loader<MainPresenter>, presenter: MainPresenter?) {
        this.presenter = presenter!!
    }

    override fun onLoaderReset(loader: Loader<MainPresenter>) {}

    /**
     * UI methods
     */
    override fun initMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        (map as SupportMapFragment).getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setOnMarkerClickListener {
            presenter.onMarkerClick(it.tag as Int)
            true
        }

        mMap.setOnMapClickListener { presenter.onMapClick() }

        //Map is ready, time to load markers
        presenter.loadMarkers()

        //Now we should show description which was shown before, so we are notifying presenter that map is ready
        presenter.mapReady()
    }

    override fun showLoadingError() {
        AlertDialog.Builder(this)
                .setTitle(getString(R.string.error_dialog_title))
                .setMessage(getString(R.string.error_dialog_message))
                .setPositiveButton(getString(R.string.quit)) { dialog, _ ->
                    dialog.dismiss()
                    this@MainActivity.finish()
                }
                .setCancelable(false)
                .show()
    }

    override fun addMarker(id: Int, markerOptions: MarkerOptions) {
        val marker = mMap.addMarker(markerOptions)
        marker.tag = id
    }

    override fun centerMap(position: LatLng) {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(position))
    }

    override fun showDescription(title: String, description: String) {

        titleTextView.text = title
        descriptionTextView.text = description

        descriptionBlock.visibility = View.VISIBLE
    }

    override fun hideDescription() {
        descriptionBlock.visibility = View.GONE
    }
}
