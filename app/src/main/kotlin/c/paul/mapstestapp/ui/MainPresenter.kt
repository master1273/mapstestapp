package c.paul.mapstestapp.ui

import c.paul.mapstestapp.backend.Client
import c.paul.mapstestapp.backend.model.Point
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Paul Chernenko
 * on 22.06.2018.
 */
class MainPresenter: MainContract.Presenter {

    private lateinit var view: MainContract.View
    private val compositeDisposable = CompositeDisposable()
    private val markerList = ArrayList<Point>()
    private var activeMarkerId: Int = -1

    override fun attach(view: MainContract.View) {
        this.view = view
        view.initMap()
    }

    override fun detach() {
        compositeDisposable.clear()
    }

    override fun mapReady() {
        onMarkerClick(activeMarkerId)
    }

    override fun loadMarkers(){
        if (markerList.isEmpty())
            compositeDisposable.add(
                    Client.getInstance().getMarkers(2)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                    {
                                        list -> run {
                                        markerList.addAll(list)
                                        showMarkersOnMap()
                                    }
                                    },
                                    {
                                        _ -> run {
                                        view.showLoadingError()
                                    }})
            )
        else
            showMarkersOnMap()
    }

    private fun showMarkersOnMap(){
        markerList.forEach {
            val position = LatLng(it.latitude, it.longitude)
            val markerOptions = MarkerOptions()
                    .position(position)
                    .title(it.title)
                    .snippet(it.description)
            view.addMarker(it.id, markerOptions)
        }
    }

    override fun onMarkerClick(id: Int) {
        if (markerList.isNotEmpty()){
            markerList.forEach {
                if (it.id == id) {
                    activeMarkerId = it.id
                    view.showDescription(it.title, it.description)
                    view.centerMap(LatLng(it.latitude, it.longitude))
                    return@forEach
                }
            }
        }
    }

    override fun onMapClick() {
        view.hideDescription()
        activeMarkerId = -1
    }
}