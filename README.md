Create Android application to display points on the map.

The information should be downloaded from server in JSON format. URL: http://www.json-generator.com/api/json/get/bPvorlFkwO?indent=2

Information about the coordinates of each point is in lat and lng fields.

Points should be displayed with standard pins.

By clicking on the pin, a panel with the text containing description should appear at the bottom of the screen. (like in Google Maps)

Min SDK: 16, Target SDK: 25. The application must support portrait and landscape orientation.